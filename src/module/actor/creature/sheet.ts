import { CreatureMythras } from '@actor'
import { ActorSheetBase } from '@actor'

export abstract class CreatureSheetMythras<TActor extends CreatureMythras>
  extends ActorSheetBase<TActor> {
}
